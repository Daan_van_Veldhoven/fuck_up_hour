#include "buffer.h"
// implementation for a buffer with ints

// shared variables within this file
static int head = 0; //Write counter of buffer
static int tail = 0; //Read counter of buffer

static int extern_buffer[MAXLEN] = {0}; //Data buffer


static bool full = false;

bool buffer_put(int p)
{
	if(full){
		printf("No action: buffer is full\r\n");
		return false; //Buffer is full
	}
	extern_buffer[head%MAXLEN] = p;
	head++;
	if(tail == ((head)%MAXLEN)){
		full = true;
	} else {

	}
	/*
	if(head > MAXLEN){
		tail++;
	}
	*/
	return true;

}

bool buffer_get(int *p)
{
	if(tail == head && !full){
		printf("No action: buffer is empty\r\n");
		return false; //Buffer is empty (no new values)
	}
	*p = extern_buffer[tail%MAXLEN];
	tail++;
	if(full){
		full = false;
	}
	return true;
}

bool buffer_is_full(void)
{
    return full;
}

bool buffer_is_empty(void)
{
    return !full;
}
