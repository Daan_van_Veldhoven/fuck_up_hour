// Program temperatuur_logger used for course EMS30 at Rotterdam University of Applied Sciences
// Harry Broeders 28-02-2020
// Every second the temperature is measured by the producer thread and stored in a buffer
// If a client connects to port 1000 via UDP, then:
//    If the UDP message contains "temp?", then:
//      while there is no temperature available in the buffer:
//          wait for 0.5 s
//      Read a temperature from the buffer and send back to the client via UDP
//    Else:
//        A "?" is send back to the client via UDP

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>

#include <ti/sysbios/BIOS.h>
#include <ti/drivers/SPI.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/net/wifi/simplelink.h>
#include <ti/drivers/net/wifi/slnetifwifi.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <ti/net/slnetutils.h>

#include "Board.h"
#include "buffer.h"

#define TMP006_ADDR (0x41) // I2C address of TMP006
#define TMP006_REG (0x0001) // Die Temp Result Register for TMP006

#define THREADSTACKSIZE (2048)
#define APPLICATION_NAME ("temperatuur_logger")
#define APPLICATION_VERSION ("1.0")
#define DEVICE_ERROR ("Device error, please refer \"DEVICE ERRORS CODES\" section in errors.h")
#define WLAN_ERROR ("WLAN error, please refer \"WLAN ERRORS CODES\" section in errors.h")
#define SL_STOP_TIMEOUT (200)

#define UDPPORT (1000)
#define UDPPACKETSIZE (32)
#define SPAWN_TASK_PRIORITY (9)
#define TASK_STACK_SIZE (2048)
#define SLNET_IF_WIFI_PRIO (5)
#define SLNET_IF_WIFI_NAME "CC32xx"

#define SSID_NAME "AndroidDV" // AP SSID
#define SECURITY_TYPE SL_WLAN_SEC_TYPE_WPA_WPA2 // Security type could be SL_WLAN_SEC_TYPE_OPEN
#define SECURITY_KEY "Enkjelftw" // Password of the secured AP

// Global mutex protecting the buffer
pthread_mutex_t m_buffer;
// Global mode of NWP (Network Processor)
int32_t mode;
// Global flags to check WLAN connection
volatile bool has_WLAN_connection = false;
volatile bool has_IP_address = false;

// check return value of call to std C function
void check_errno(int error)
{
    if (error < 0)
    {
        perror("Error");
        exit(EXIT_FAILURE);
    }
}

// check return value of call to pthread function
void check(int error)
{
    if (error != 0)
    {
        check_errno( printf("Error: %s\n", strerror(error)) );
        exit(EXIT_FAILURE);
    }
}

// check return value of call to socket_layer (sl)
void check_sl(int error, int line, char *msg)
{
    if (error < 0)
    {
        check_errno( printf("\n[line:%d, error code:%d] %s\n", line, error, msg) );
    }
}

int msleep(long msec) // millisecond sleep
{
    struct timespec ts;
    ts.tv_sec = msec / 1000;
    ts.tv_nsec = (msec % 1000) * 1000000;
    return nanosleep(&ts, NULL);
}

void* consumer(void *arg)
{
    (void) arg;
    socklen_t addrlen;
    char buffer[UDPPACKETSIZE];

    printf("Waiting for client\n");

    check_errno( socket(AF_INET, SOCK_DGRAM, 0) );

    struct sockaddr_in localAddr;
    memset(&localAddr, 0, sizeof(localAddr));
    localAddr.sin_family = AF_INET;
    localAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    localAddr.sin_port = htons(UDPPORT);

    int fd_server = 0;
    check_errno( bind(fd_server, (struct sockaddr *)&localAddr, sizeof(localAddr)) );

    while (1) {
        fd_set readSet;
        FD_ZERO(&readSet);
        FD_SET(fd_server, &readSet);
        struct sockaddr_in clientAddr;
        addrlen = sizeof(clientAddr);

        // Wait forever for a request
        check_errno( select(fd_server + 1, &readSet, NULL, NULL, NULL) );
        if (FD_ISSET(fd_server, &readSet))
        {
            check_errno( recvfrom(fd_server, buffer, UDPPACKETSIZE,
                                  0, (struct sockaddr *)&clientAddr, &addrlen) );
            check_errno( printf("Received UDP packet from: IP = %d.%d.%d.%d, PORT = %d\n",
               SL_IPV4_BYTE(ntohl(clientAddr.sin_addr.s_addr), 3),
               SL_IPV4_BYTE(ntohl(clientAddr.sin_addr.s_addr), 2),
               SL_IPV4_BYTE(ntohl(clientAddr.sin_addr.s_addr), 1),
               SL_IPV4_BYTE(ntohl(clientAddr.sin_addr.s_addr), 0),
               ntohs(clientAddr.sin_port)) );
            check_errno( printf("Received: %s\n", buffer) );
            if (strcmp(buffer, "temp?") == 0)
            {
                check( pthread_mutex_lock(&m_buffer) );
                while (buffer_is_empty())
                {
                    check( pthread_mutex_unlock(&m_buffer) );
                    check_errno( msleep(500) );
                    check( pthread_mutex_lock(&m_buffer) );
                }
                int temp; // temperature in tenthes of degrees C
                buffer_get(&temp);
                check( pthread_mutex_unlock(&m_buffer) );
                check_errno( sprintf(buffer, "%d", temp) );
            }
            else
            {
                check_errno( sprintf(buffer, "?") );
            }
            check_errno( sendto(fd_server, buffer, strlen(buffer) + 1,
                                0, (struct sockaddr *)&clientAddr, addrlen) );
            check_errno( printf("Send %s\n", buffer) );
        }
    }
    // return NULL;
}

void SimpleLinkNetAppEventHandler(SlNetAppEvent_t *pNetAppEvent)
{
    if (pNetAppEvent != NULL && pNetAppEvent->Id == SL_NETAPP_EVENT_IPV4_ACQUIRED)
    {
        has_IP_address = true;
        // Initialize SlNetSock layer with CC3x20 interface
        check_sl( SlNetIf_init(0), __LINE__, DEVICE_ERROR );
        check_sl( SlNetIf_add(SLNETIF_ID_1, SLNET_IF_WIFI_NAME,
                              (const SlNetIf_Config_t *)&SlNetIfConfigWifi,
                              SLNET_IF_WIFI_PRIO),
                  __LINE__, DEVICE_ERROR );
        check_sl( SlNetSock_init(0), __LINE__, DEVICE_ERROR );
        check_sl( SlNetUtil_init(0), __LINE__, DEVICE_ERROR );

        if (mode != ROLE_AP)
        {
            check_errno( printf("IP Acquired: IP = %d.%d.%d.%d, "
               "Gateway = %d.%d.%d.%d\n",
               SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,3),
               SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,2),
               SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,1),
               SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Ip,0),
               SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Gateway,3),
               SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Gateway,2),
               SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Gateway,1),
               SL_IPV4_BYTE(pNetAppEvent->Data.IpAcquiredV4.Gateway,0)) );

            pthread_attr_t attr;
            check( pthread_attr_init(&attr) );
            struct sched_param sched_par;
            sched_par.sched_priority = 1;
            check( pthread_attr_setschedparam(&attr, &sched_par) );
            check( pthread_attr_setstacksize(&attr, TASK_STACK_SIZE) );
            check( pthread_create(NULL, &attr, consumer, NULL) );
        }
    }
}

void SimpleLinkFatalErrorEventHandler(SlDeviceFatal_t *slFatalErrorEvent)
{
    // Unused in this application
}

void SimpleLinkNetAppRequestMemFreeEventHandler(uint8_t *buffer)
{
    // Unused in this application
}

void SimpleLinkNetAppRequestEventHandler(SlNetAppRequest_t *pNetAppRequest,
                                         SlNetAppResponse_t *pNetAppResponse)
{
    // Unused in this application
}

void SimpleLinkHttpServerEventHandler(SlNetAppHttpServerEvent_t *pHttpEvent,
                                      SlNetAppHttpServerResponse_t *pHttpResponse)
{
    // Unused in this application
}

void SimpleLinkWlanEventHandler(SlWlanEvent_t *pWlanEvent)
{
    switch(pWlanEvent->Id)
    {
    case SL_WLAN_EVENT_CONNECT:
        check_errno( printf("Connected to WLAN.\n") );
        has_WLAN_connection = true;
        break;
    case SL_WLAN_EVENT_DISCONNECT:
        check_errno( printf("Disconnected from WLAN: %d.\n", (int)pWlanEvent->Data.Disconnect.ReasonCode) );
        has_WLAN_connection = false;
        break;
    default:
        check_errno( printf("Unknown error while connecting to WLAN.\n") );;
    }
}

void SimpleLinkGeneralEventHandler(SlDeviceEvent_t *pDevEvent)
{
    // Unused in this application
}

void SimpleLinkSockEventHandler(SlSockEvent_t *pSock)
{
    // Unused in this application
}

void *producer(void *arg) // function for producer thread
{
    (void)arg;

    I2C_init();
    I2C_Params i2cParams;
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_400kHz;
    I2C_Handle i2c = I2C_open(Board_I2C_TMP, &i2cParams);
    if (i2c == NULL)
    {
        check_errno( printf("Error: Initializing I2C\n") );
        exit(EXIT_FAILURE);
    }
    else
    {
        check_errno( printf("I2C Initialized!\n") );
    }

    uint8_t txBuffer[1];
    uint8_t rxBuffer[2];
    I2C_Transaction i2cTransaction;
    i2cTransaction.writeBuf = txBuffer;
    i2cTransaction.writeCount = 1;
    i2cTransaction.readBuf = rxBuffer;
    i2cTransaction.readCount = 2;

    txBuffer[0] = TMP006_REG;
    i2cTransaction.slaveAddress = TMP006_ADDR;
    if (!I2C_transfer(i2c, &i2cTransaction))
    {
        check_errno( printf("Error: No TMP sensor found!\n") );
        exit(EXIT_FAILURE);
    }
    else
    {
        check_errno( printf("Detected TMP006 sensor.\n") );
    }

    while (1)
    {
        int16_t temperature;  // temperature in tenthes of degrees C
        if (I2C_transfer(i2c, &i2cTransaction))
        {
            temperature = (rxBuffer[0] << 8) | (rxBuffer[1]);
            temperature *= 0.078125;
            check_errno( printf("Temperature sampled: %d/10 degrees Celsius\n", temperature) );
        }
        else
        {
            check_errno( printf("I2C Bus fault.\n") );
        }

        check( pthread_mutex_lock(&m_buffer) );
        if (!buffer_is_full())
        {
            buffer_put(temperature);
        }
        check( pthread_mutex_unlock(&m_buffer) );
        check_errno( msleep(1000) );
    }

    // I2C_close(i2c);
    // check_errno( printf("I2C closed!\n") );
    // return NULL;
}

void *server(void *arg) // function for consumer thread
{
    (void) arg;

    SPI_init();

    pthread_attr_t attr;
    check( pthread_attr_init(&attr) );
    struct sched_param sched_par;
    sched_par.sched_priority = SPAWN_TASK_PRIORITY;
    check( pthread_attr_setschedparam(&attr, &sched_par) );
    check( pthread_attr_setstacksize(&attr, TASK_STACK_SIZE) );
    check( pthread_create(NULL, &attr, sl_Task, NULL) );

    // Turn NWP on - initialize the device
    check_sl( mode = sl_Start(0, 0, 0), __LINE__, DEVICE_ERROR );
    if (mode != ROLE_STA)
    {
        // Set NWP role as STA
        check_sl( mode = sl_WlanSetMode(ROLE_STA), __LINE__, WLAN_ERROR );
        // For changes to take affect, we restart the NWP
        check_sl( sl_Stop(SL_STOP_TIMEOUT), __LINE__, DEVICE_ERROR );
        check_sl( mode = sl_Start(0, 0, 0), __LINE__, DEVICE_ERROR );
    }

    if (mode != ROLE_STA)
    {
        check_errno( printf("[line:%d, error code:%d] %s\n", __LINE__, mode, DEVICE_ERROR) );
    }

    SlWlanSecParams_t secParams = {0};
    secParams.Key = (signed char*)SECURITY_KEY;
    secParams.KeyLen = strlen(SECURITY_KEY);
    secParams.Type = SECURITY_TYPE;
    check_errno( printf("Connecting to: %s.\n", SSID_NAME) );
    check_sl( sl_WlanConnect((signed char*)SSID_NAME, strlen(SSID_NAME), 0, &secParams, 0),
              __LINE__, DEVICE_ERROR);
    // Wait for ~10 sec to check if connection to desire AP succeeds
    int wait_for_connection = 0;
    while (wait_for_connection < 20 && !(has_WLAN_connection && has_IP_address))
    {
        check_errno( msleep(500) );
        wait_for_connection++;
    }
    if (!has_WLAN_connection)
    {
        check_errno( printf("Error: connection to: %s failed (check SSID and password)\n", SSID_NAME) );
    }
    else if (!has_IP_address)
    {
        check_errno( printf("Error: no IP received from: %s (check DHCP server)\n", SSID_NAME) );
    }
    return NULL;
}

void *main_thread(void *arg)
{
    (void)arg;

    pthread_mutexattr_t ma;
    check( pthread_mutexattr_init(&ma) );
    check( pthread_mutex_init(&m_buffer, &ma) );

    pthread_attr_t attrs;
    check( pthread_attr_init(&attrs) );
    check( pthread_attr_setstacksize(&attrs, THREADSTACKSIZE) );

    pthread_t server_thread, producer_thread;
    check( pthread_create(&server_thread, &attrs, server, NULL) );
    check( pthread_create(&producer_thread, &attrs, producer, NULL) );

    check( pthread_join(server_thread, NULL) );
    check( pthread_join(producer_thread, NULL) );

    check( pthread_mutex_destroy(&m_buffer) );
    check( pthread_mutexattr_destroy(&ma) );

    return NULL;
}

int main(void)
{
    Board_initGeneral();

    pthread_attr_t attrs;
    check( pthread_attr_init(&attrs) );
    check( pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_DETACHED) );
    check( pthread_attr_setstacksize(&attrs, THREADSTACKSIZE) );

    struct sched_param priParam;
    priParam.sched_priority = 15;
    check( pthread_attr_setschedparam(&attrs, &priParam) );
    check( pthread_create(NULL, &attrs, main_thread, NULL) );

    BIOS_start();
    return EXIT_SUCCESS;
}
